﻿using webbanhang.Models;

namespace webbanhang.Repositories
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAllCategories();
    }
}
